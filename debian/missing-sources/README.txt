echarts.common.js
  https://github.com/apache/echarts/tree/5.0.0/dist
  plain and minified files have different copyrights?

jquery-1.11.3.js
  https://github.com/jquery/jquery/tree/1.11.3/dist

jquery.jqote2.js
  https://raw.githubusercontent.com/aefxx/jQote2/69b2053a13f5f180e696de9b3dba856a3c00678c/jquery.jqote2.js

jquery.tools.js
  https://github.com/jquerytools/jquerytools/blob/v1.2.5/src/tabs/tabs.js
  This is the closest to the source i could find, i determined it was Tabs from the `[tabs]` tag in the minified version
